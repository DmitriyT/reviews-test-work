var gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));

gulp.task('build', function(){ // Создаем таск "sass"
    return gulp.src('app/sass/tw_reviews.sass') // Берем источник
        .pipe(sass())
        .pipe(gulp.dest('css/')) // Выгружаем результата в папку app/css
});