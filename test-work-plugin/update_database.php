<?php
defined( 'ABSPATH' ) or die();

class tw_reviews_update_database {

	protected static $instance;

	public function __construct() {

		add_action('rest_api_init', [$this, 'add_review_route']);

	}

	public function add_review_route(){
		register_rest_route('review/v1/reviews', 'addReview', [
			'methods' => 'POST',
			'callback' => [$this, 'add_review_function']
		] );
	}

	public function add_review_function($request) {

		$params = $request->get_params();

		$new_post_id = $this->create_request( $params );

		if ( $new_post_id ) {
			$update = $this->update_database( $params, $new_post_id );

			if ( $update ) {
				$response = $this->format_response( 'Send success', 'success' );
			} else {
				$response = $this->format_response( 'Send failed', 'error' );
			}

		} else {
			$response = $this->format_response( 'Send failed', 'error' );
		}

		echo json_encode( $response );

		die();

	}

	private function format_response( $message, $status ) {
		return [
			'message' => __( $message, 'tw_reviews' ),
			'status'  => $status
		];
	}

	private function create_request( $update_dates ) {

		if ( empty( $update_dates ) || ! is_array( $update_dates ) ) {
			return false;
		}

		if(!isset($update_dates['title']) || empty($update_dates['title'])){
			return false;
		}

		$post_data = array(
			'post_title'   => sanitize_text_field($update_dates['title']),
			'post_content' => $update_dates['review'],
			'post_status'  => 'publish',
			'post_author'  => 1,
			'post_type'    => 'tw_reviews',
		);

		return wp_insert_post( $post_data );
	}

	private function update_database( $update_datas, $post_id ) {

		if ( empty( $update_datas ) || ! is_array( $update_datas ) || empty( $post_id ) ) {
			return false;
		}

		foreach ( $update_datas as $key => $value ) {

			if ( $key == 'review' || $key == 'title' ) {
				continue;
			}

			update_post_meta( $post_id, 'reviews_' . $key, $value );
		}

		return true;

	}

	public static function get_instance() {
		if ( null === self::$instance ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

}

tw_reviews_update_database::get_instance();