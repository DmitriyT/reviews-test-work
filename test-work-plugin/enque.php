<?php
defined( 'ABSPATH' ) or die();

class tw_reviews_enque {

	protected static $instance;

	private function __construct() {

		add_action( 'wp_enqueue_scripts', array( $this, 'plugin_styles' ) );


	}

	public function plugin_styles() {
		wp_enqueue_style( 'tw_reviews', plugin_dir_url( __FILE__ ) . 'css/tw_reviews.css', array(), '1.0.0', 'all' );
		wp_enqueue_script( 'tw_reviews', plugin_dir_url( __FILE__ ) . 'js/tw_reviews.js', array( 'jquery' ), '1.0.0', true );
		wp_localize_script( 'tw_reviews', 'tw_reviews',
			array(
				'rest_url' => rest_url() . 'review/v1/reviews/addReview',
				'url'   => admin_url( 'admin-ajax.php' ),
			)
		);
	}

	public static function get_instance() {
		if ( null === self::$instance ) {
			self::$instance = new self();
		}

		return self::$instance;
	}
}

tw_reviews_enque::get_instance();