<?php
/*
 * Plugin Name: Reviews (test work)
 * Description: Plugin for test work
 * Version: 1.1.1
 * Author: Dmitriy Tatanov
 * Text Domain: tw_reviews
 */
require_once plugin_dir_path( __FILE__ ) . '/enque.php';
require_once plugin_dir_path( __FILE__ ) . '/shortcodes.php';
require_once plugin_dir_path( __FILE__ ) . '/tw_reviews_cpt.php';
require_once plugin_dir_path( __FILE__ ) . '/update_database.php';

function form_page_example() {

	$page_data = array(
		'post_title'   => wp_strip_all_tags( 'Page With Form' ),
		'post_content' => '[tw_render_form]',
		'post_status'  => 'publish',
		'post_type'    => 'page',
	);

	wp_insert_post( $page_data );

}

register_activation_hook( __FILE__, 'form_page_example' );

function tw_reviews_clear() {

	$activation_page = get_page_by_title( 'Page With Form' );

	if ( $activation_page ) {

		$id = $activation_page->ID;
		wp_delete_post( $id );

	}

	global $wpdb;

	$wpdb->query( $wpdb->prepare( "DELETE a,b,c FROM $wpdb->posts a
    LEFT JOIN $wpdb->term_relationships b
        ON (a.ID = b.object_id)
    LEFT JOIN $wpdb->postmeta c
        ON (a.ID = c.post_id)
    WHERE a.post_type = '%s';", 'tw_reviews' ) );

}

register_uninstall_hook( __FILE__, 'tw_reviews_clear' );
