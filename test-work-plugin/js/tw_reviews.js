(function ($) {

    $(document).ready(function () {

        $('.tw_reviews_form form').on('submit', function (e) {

            e.preventDefault();

            let form = $(this);

            form.addClass('active');

            let name = form.find('input[name="name"]').val();
            let title = form.find('input[name="title"]').val();
            let social_network = form.find('input[name="social_network"]').val();
            let review = form.find('textarea[name="review"]').val();

            var data = {
                name: name,
                title: title,
                social_network: social_network,
                review: review
            }

            $.ajax({
                type: 'POST',
                url: tw_reviews.rest_url,
                data: data,
                dataType: 'json',
                success: function (response, status) {
                    console.log(response);
                    form.removeClass('active');
                    form.addClass('success');
                    form.trigger("reset");
                },
                error: function (jqXhr, textStatus, errorMessage) {
                    console.log('error');
                    form.trigger("reset");
                    form.addClass('error');
                    form.removeClass('active');
                    console.log(jqXhr);
                    console.log(textStatus);
                    console.log(errorMessage);
                }
            });

        });

    });

})(jQuery);