<?php
defined( 'ABSPATH' ) or die();

class tw_reviews_cpt {

	protected static $instance;

	private function __construct() {

		add_action( 'init', array( $this, 'register_tw_reviews' ) );
		add_action( 'add_meta_boxes', array( $this, 'tw_reviews_metabox' ) );
		add_action( 'save_post', array( $this, 'tw_reviews_datas_save' ), 10, 2 );

	}


	public function register_tw_reviews() {

		$labels = array(
			'name'               => _x( 'Review', 'tw_reviews' ),
			'singular_name'      => _x( 'Review', 'tw_reviews' ),
			'menu_name'          => __( 'Reviews', 'tw_reviews' ),
			'parent_item_colon'  => __( 'Reviews', 'tw_reviews' ),
			'all_items'          => __( 'All Reviews', 'tw_reviews' ),
			'view_item'          => __( 'View Review', 'tw_reviews' ),
			'add_new_item'       => __( 'Add New Review', 'tw_reviews' ),
			'add_new'            => __( 'Add New', 'tw_reviews' ),
			'edit_item'          => __( 'Edit Review', 'tw_reviews' ),
			'update_item'        => __( 'Update Review', 'tw_reviews' ),
			'search_items'       => __( 'Search Review', 'tw_reviews' ),
			'not_found'          => __( 'Not Found', 'tw_reviews' ),
			'not_found_in_trash' => __( 'Not found in Trash', 'tw_reviews' ),
		);

		$args = array(
			'label'               => __( 'Reviews', 'tw_reviews' ),
			'description'         => __( 'Reviews', 'tw_reviews' ),
			'labels'              => $labels,
			'menu_icon'           => 'dashicons-welcome-widgets-menus',
			'show_in_rest'        => true,
			'supports'            => array( 'title', 'editor' ),
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 5,
			'can_export'          => true,
			'has_archive'         => false,
			'exclude_from_search' => true,
			'publicly_queryable'  => false,
			'capability_type'     => 'post',
		);

		register_post_type( 'tw_reviews', $args );

	}

	public function tw_reviews_metabox() {
		add_meta_box(
			'tw_reviews_datas',
			'Request datas',
			array( $this, 'tw_reviews_datas_callback' ),
			'tw_reviews',
			'normal',
			'default'
		);
	}

	public function tw_reviews_datas_callback( $post ) {
		$name  = get_post_meta( $post->ID, 'reviews_name', true );
		$social_network  = get_post_meta( $post->ID, 'reviews_social_network', true );

		?>
        <div class="callback-meta-box"
             style="display: flex; align-items: center; justify-content: space-between; flex-wrap: wrap;">
            <div style="max-width: 45%; width: 100%;padding: 15px;">
                <input style="width: 100%;" type="text" name="reviews_name" value="<?php echo esc_attr( $name ); ?>">
            </div>

            <div style="max-width: 45%; width: 100%;padding: 15px;">
                <input style="width: 100%;" type="text" name="reviews_social_network"
                       value="<?php echo esc_attr( $social_network ); ?>">
            </div>

        </div>
		<?php
	}

	public function tw_reviews_datas_save( $post_id, $post ) {

		$post_type = get_post_type_object( $post->post_type );

		if ( ! current_user_can( $post_type->cap->edit_post, $post_id ) ) {
			return $post_id;
		}

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return $post_id;
		}

		if ( isset( $_POST['reviews_name'] ) ) {
			update_post_meta( $post_id, 'reviews_name', sanitize_text_field( $_POST['reviews_name'] ) );
		} else {
			delete_post_meta( $post_id, 'reviews_name' );
		}

		if ( isset( $_POST['reviews_social_network'] ) ) {
			update_post_meta( $post_id, 'reviews_social_network', $_POST['reviews_social_network'] );
		} else {
			delete_post_meta( $post_id, 'reviews_social_network' );
		}

		return $post_id;

	}

	public static function get_instance() {
		if ( null === self::$instance ) {
			self::$instance = new self();
		}

		return self::$instance;
	}
}

tw_reviews_cpt::get_instance();