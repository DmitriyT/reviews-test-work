<?php
defined( 'ABSPATH' ) or die();

class tw_reviews_shortcodes {

	protected static $instance;

	private function __construct() {

		add_shortcode( 'tw_render_form', array( $this, 'tw_render_form_function' ) );
		add_action( 'render_form', array( $this, 'render_form_html' ) );

	}

	public function tw_render_form_function() {

		ob_start();

		do_action( 'render_form' );

		$html = ob_get_contents();
		ob_end_clean();

		return $html;

	}

	public function render_form_html() {
		?>
        <div class="tw_reviews_from_wrapper">
            <div class="tw_reviews_form">
                <form>
                    <div class="loader">
                        <img src="<?php echo esc_url( plugin_dir_url( __FILE__ ) . 'img/480px-Loader.gif' ); ?>"
                             alt="<?php esc_attr_e( 'Loading...', 'tw_reviews' ); ?>">
                    </div>
                    <p><input required type="text" name="title"
                              placeholder="<?php echo esc_attr__( 'Title', 'tw_reviews' ); ?>"></p>
                    <p><input type="text" required name="name"
                              placeholder="<?php echo esc_attr__( 'Name', 'tw_reviews' ); ?>"></p>
                    <p><textarea name="review" cols="30" rows="10"
                                 placeholder="<?php echo esc_attr( 'Your Review', 'tw_reviews' ); ?>"></textarea></p>
                    <p><input type="text" required name="social_network"
                              placeholder="<?php echo esc_attr__( 'Social Network', 'tw_reviews' ); ?>"></p>

                    <p class="succes_text"><?php echo esc_html__( 'Your message send successful', 'tw_reviews' ); ?></p>
                    <p class="error_text"><?php echo esc_html__( 'Something wrong. Plz try again', 'tw_reviews' ); ?></p>

                    <input type="submit" name="submit" value="<?php echo esc_attr( 'Submit Review', 'tw_reviews' ) ?>">
                </form>
            </div>
        </div>
		<?php
	}

	public static function get_instance() {
		if ( null === self::$instance ) {
			self::$instance = new self();
		}

		return self::$instance;
	}
}

tw_reviews_shortcodes::get_instance();